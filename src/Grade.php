<?php
/**
 * Created by PhpStorm.
 * User: Mazed
 * Date: 3/22/2018
 * Time: 6:26 AM
 */

namespace Pondit\Calculator\GradeCalculator;


class Grade
{
    public $marks;
    public $getcgpa;

   public function __construct($mark,$getcgpa)
    {
        $this->marks=$mark;
        $this->getcgpa=$getcgpa;
    }


    public function  getGPA()
    {
        $this->marks;
        if($this->marks>=80)
        {
            return 'A+';
        }
        else if($this->marks>=70)
        {
            return 'A';
        }
        else if($this->marks>60)
        {
            return 'A-';
        }
        else if($this->marks>=50)
        {
            return 'f';
        }
    }

    public function getCGPA()
    {
        $this->getcgpa;
        $totalMarks=array_sum($this->getcgpa);
        $avarage=$totalMarks/count($this->getcgpa);
         if($avarage>=80)
         {
             return 'A+';
         }
         else if($avarage>=70)
         {
             return 'A';
         }

         else if($avarage>=60)
         {
             return 'B';
         }

         else if($avarage>=50)
         {
             return 'F';
         }

    }
}